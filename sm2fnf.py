import json
import midiutil
import sys
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
import datetime
import traceback


# Config:

# ticks in one quarter note
chart_resolution = 480

# usually a 16th note duration, short enough to not be considered a sustain
short_duration = round(480 / 4)

# should hold notes be a press+release pair of notes or one long note?
holds_as_lifts = False


# Constants:
app_version = "0.2.0"
INSTRUMENTS = ["Lead", "Drums", "Bass", "Vocal"]
DIFFICULTIES = ["Easy", "Medium", "Hard", "Expert"]


def read_until(text, position_ref, delimeters, accept_eof=False):
    initial_position = position_ref["value"]
    current_position = initial_position
    while current_position < len(text):
        if text[current_position] in delimeters:
            position_ref["value"] = current_position
            return text[initial_position:current_position]
        current_position += 1

    if accept_eof:
        position_ref["value"] = current_position
        return text[initial_position:current_position]
    else:
        return None


def parse_bpms(bpms):
    result = []
    bpm_changes = bpms.split(",")
    for bpm_change in bpm_changes:
        stripped_bpm_change = bpm_change.strip()
        (beat, bpm) = stripped_bpm_change.split("=")
        result.append({"beat": float(beat), "bpm": float(bpm)})
    return result


def parse_note_chunk(note_chunk):
    [chart_type, charter, difficulty, difficulty_num, mystery_numbers, note_data] = [
        x.strip() for x in note_chunk.split(":")
    ]

    notes = []

    active_sustains = [None, None, None, None]

    bars_data = note_data.split(",")
    for bar_index, bar_data in enumerate(bars_data):
        beats_data = list(filter(lambda beat: beat != "", bar_data.split("\n")))
        beat_count = len(beats_data)

        for beat_index, beat_data in enumerate(beats_data):
            fraction_of_bar = beat_index / beat_count
            position_in_bars = bar_index + fraction_of_bar
            tick = round(position_in_bars * 4 * chart_resolution)

            for lane in range(4):
                if beat_data[lane] == "0":
                    # No note here, ignore
                    pass
                elif beat_data[lane] == "1":
                    # Normal note
                    notes.append({"tick": tick, "lane": lane})
                elif beat_data[lane] == "2":
                    # Start of hold note
                    if holds_as_lifts:
                        notes.append({"tick": tick, "lane": lane})
                    else:
                        active_sustains[lane] = tick
                elif beat_data[lane] == "3":
                    # End of hold note
                    if holds_as_lifts:
                        notes.append({"tick": tick, "lane": lane, "lift": True})
                    else:
                        if active_sustains[lane] == None:
                            print(
                                "warning: found ending of a nonexistent sustain; charting lift note instead"
                            )
                            notes.append({"tick": tick, "lane": lane, "lift": True})
                        else:
                            start_tick = active_sustains[lane]
                            active_sustains[lane] = None
                            notes.append(
                                {
                                    "tick": start_tick,
                                    "lane": lane,
                                    "duration": tick - start_tick,
                                }
                            )

    return {
        "chart_type": chart_type,
        "charter": charter,
        "difficulty": difficulty,
        "difficulty_num": difficulty_num,
        "mystery_numbers": mystery_numbers,
        "notes": notes,
    }


def parse_sm_text(sm_text):
    metadata = {}
    timing = []
    charts = []
    other_data = []

    position_ref = {"value": 0}
    while position_ref["value"] < len(sm_text):
        whitespace = read_until(sm_text, position_ref, ["#"])
        position_ref["value"] += 1
        key = read_until(sm_text, position_ref, [":"])
        position_ref["value"] += 1
        value = read_until(sm_text, position_ref, [";", "#"], accept_eof=True)

        if key is not None:
            if key in [
                "TITLE",
                "SUBTITLE",
                "ARTIST",
                "TITLETRANSLIT",
                "SUBTITLETRANSLIT",
                "ARTISTTRANSLIT",
                "GENRE",
                "CREDIT",
                "MUSIC",
                "BANNER",
                "BACKGROUND",
                "CDTITLE",
                "SAMPLESTART",
                "SAMPLELENGTH",
                "SELECTABLE",
                "OFFSET",
                "STOPS",
                "BGCHANGES",
                "FGCHANGES",
            ]:
                metadata[key] = value

            elif key == "BPMS":
                timing = parse_bpms(value)

            elif key == "NOTES":
                chart = parse_note_chunk(value)
                charts.append(chart)

            else:
                other_data.append({"key": key, "value": value})

    song = {"metadata": metadata, "timing": timing, "charts": charts}

    return song


def load_sm(filename):
    with open(filename) as f:
        text = f.read()
        return parse_sm_text(text)


def export_track(mf, track_id, track_name, timing, all_charts):
    time = 0
    mf.addTrackName(track_id, time, track_name)

    for tempo_change in timing:
        mf.addTempo(track_id, tempo_change["beat"], tempo_change["bpm"])

    note_ranges = {"Easy": 60, "Medium": 72, "Hard": 84, "Expert": 96}
    lift_marker_offset = 6
    overdrive_phrase_pitch = 116

    channel = 0
    velocity = 100

    # Add notes
    for difficulty in DIFFICULTIES:
        if difficulty not in all_charts or all_charts[difficulty] is None:
            continue

        chart = all_charts[difficulty]
        base_range = note_ranges[difficulty]

        for note in chart["notes"]:
            pitch = base_range + note["lane"]
            time = note["tick"] / chart_resolution
            duration_ticks = (
                note["duration"] if ("duration" in note) else short_duration
            )
            duration = duration_ticks / chart_resolution
            mf.addNote(track_id, channel, pitch, time, duration, velocity)
            if "lift" in note and note["lift"]:
                mf.addNote(
                    track_id,
                    channel,
                    pitch + lift_marker_offset,
                    time,
                    duration,
                    velocity,
                )


def export_midi(song, output_filepath, chart_map):
    mf = midiutil.MidiFile.MIDIFile(
        4, file_format=1, ticks_per_quarternote=chart_resolution
    )

    timing = song["timing"]

    track_map = {
        "Lead": "PART GUITAR",
        "Drums": "PART DRUMS",
        "Bass": "PART BASS",
        "Vocal": "PART VOCALS",
    }

    for index, instrument in enumerate(INSTRUMENTS):
        instrument_charts = {}
        for difficulty in DIFFICULTIES:
            mapped_chart = chart_map[instrument][difficulty]
            if mapped_chart is None:
                instrument_charts[difficulty] = None
            else:
                instrument_charts[difficulty] = song["charts"][
                    chart_map[instrument][difficulty]
                ]

        export_track(mf, index, track_map[instrument], timing, instrument_charts)

    # write it to disk
    with open(output_filepath, "wb") as outf:
        mf.writeFile(outf)


def get_default_chart_map(song):
    i = 0
    chart_map = {}
    for instrument in INSTRUMENTS:
        chart_map[instrument] = {}

    for difficulty in DIFFICULTIES:
        for instrument in INSTRUMENTS:
            if song is None:
                chart_map[instrument][difficulty] = None
            else:
                if i >= len(song["charts"]):
                    i = len(song["charts"]) - 1
                chart_map[instrument][difficulty] = i
                i += 1

    return chart_map


def load_and_convert(input_filepath, output_filepath, chart_map, json_filepath=None):
    print(f"Reading {input_filepath}...")
    song = load_sm(input_filepath)

    if chart_map is None:
        chart_map = get_default_chart_map(song)

    convert(song, output_filepath, chart_map, json_filepath)


def convert(song, output_filepath, chart_map, json_filepath=None):
    if json_filepath is not None:
        with open(json_filepath, "w+") as output_file:
            json.dump(song, output_file, indent=4)

    print(f"Exporting to {output_filepath}...")
    export_midi(song, output_filepath, chart_map)
    print(f"Done")


gui_input_filepath = None
gui_output_filepath = None
gui_run_status = None
gui_chart_map_vars = None
gui_chart_map_dropdowns = None
loaded_song = None
choices = ["<none>"]


def gui_input_button(*args):
    global loaded_song, choices, gui_chart_map_dropdowns
    path = filedialog.askopenfilename()
    if path == () or path == "":
        path = None
    print(f"Input file: {path}")

    gui_input_filepath.set(path)
    gui_run_status.set("")

    loaded_song = None
    choices = ["<none>"]

    if path is not None:
        try:
            loaded_song = load_sm(path)
            for index, chart_info in enumerate(loaded_song["charts"]):
                choices.append(
                    f"{index}: {chart_info['difficulty']} [{chart_info['difficulty_num']}] \n - {chart_info['charter']}"
                )

        except Exception as e:
            gui_run_status.set(f"Error! ({datetime.datetime.now()})\n{e}")
            traceback.print_exc()

    chart_map = get_default_chart_map(loaded_song)

    for difficulty in DIFFICULTIES:
        for instrument in INSTRUMENTS:
            dropdown = gui_chart_map_dropdowns[instrument][difficulty]
            mapped_chart_index = chart_map[instrument][difficulty]
            if mapped_chart_index is None:
                dropdown.set_menu("<none>", *choices)
            else:
                dropdown.set_menu(choices[mapped_chart_index + 1], *choices)


def gui_output_button(*args):
    path = filedialog.asksaveasfilename()
    if path == "":
        path = None
    gui_output_filepath.set(path)
    print(f"Output file: {path}")


def gui_run_button(*args):
    gui_run_status.set("Converting...")
    try:
        chart_map = {}
        for instrument in gui_chart_map_vars:
            chart_map[instrument] = {}
            for difficulty in gui_chart_map_vars[instrument]:
                value = gui_chart_map_vars[instrument][difficulty].get()
                if value == "<none>":
                    chart_map[instrument][difficulty] = None
                else:
                    chart_map[instrument][difficulty] = int(value.split(":")[0])

        convert(loaded_song, gui_output_filepath.get(), chart_map)
        gui_run_status.set(f"Done! ({datetime.datetime.now()})")
    except Exception as e:
        gui_run_status.set(f"Error! ({datetime.datetime.now()})\n{e}")
        traceback.print_exc()


def start_gui():
    global gui_input_filepath, gui_output_filepath, gui_run_status, gui_chart_map_vars, gui_chart_map_dropdowns

    root = tk.Tk()
    root.title("StepMania to Fortnite Festival chart converter")
    mainframe = ttk.Frame(root, padding="12 12 12 12")
    mainframe.grid(column=0, row=0, sticky=tk.NSEW)
    mainframe.columnconfigure(0, weight=0)
    mainframe.columnconfigure(1, weight=2)
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)

    gui_input_filepath = tk.StringVar()
    gui_output_filepath = tk.StringVar()
    gui_run_status = tk.StringVar()

    tk.Button(mainframe, text="Select input file (.sm)", command=gui_input_button).grid(
        column=0, row=0, padx=5, sticky=tk.W
    )
    ttk.Label(mainframe, textvariable=gui_input_filepath).grid(
        column=1, row=0, sticky=tk.W
    )

    tk.Button(
        mainframe, text="Select output file (.mid)", command=gui_output_button
    ).grid(column=0, row=1, padx=5, sticky=tk.W)
    ttk.Label(mainframe, textvariable=gui_output_filepath).grid(
        column=1, row=1, sticky=tk.W
    )

    charts_frame = tk.LabelFrame(mainframe, text="Chart mapping")
    charts_frame.grid(column=0, row=2, columnspan=2, sticky=tk.NSEW)

    gui_chart_map_vars = {}
    gui_chart_map_dropdowns = {}
    for i, instrument in enumerate(INSTRUMENTS):
        gui_chart_map_vars[instrument] = {}
        gui_chart_map_dropdowns[instrument] = {}

        charts_frame.columnconfigure(i * 2, weight=1)
        charts_frame.columnconfigure((i * 2) + 1, weight=1)

        for j, difficulty in enumerate(reversed(DIFFICULTIES)):
            tkvar = tk.StringVar()
            tk.Label(charts_frame, text=f"{instrument}, {difficulty}").grid(
                column=(i * 2), row=j, sticky=tk.E
            )
            dropdown = ttk.OptionMenu(charts_frame, tkvar, "<none>", *choices)
            dropdown.grid(column=((i * 2) + 1), row=j)
            gui_chart_map_dropdowns[instrument][difficulty] = dropdown
            gui_chart_map_vars[instrument][difficulty] = tkvar

    tk.Button(mainframe, text="Run", command=gui_run_button).grid(
        column=0, row=3, padx=5, sticky=tk.W
    )
    ttk.Label(mainframe, textvariable=gui_run_status).grid(column=1, row=3, sticky=tk.W)
    ttk.Label(mainframe, text=f"made by TadeLn, v{app_version}").grid(
        column=0, row=4, columnspan=2
    )

    for child in charts_frame.winfo_children():
        child.grid_configure(padx=5, pady=5)

    for child in mainframe.winfo_children():
        child.grid_configure(pady=5)

    root.bind("<Control-o>", gui_input_button)
    root.bind("<Control-s>", gui_output_button)
    root.bind("<Return>", gui_run_button)

    root.mainloop()


def main(args):
    if len(args) < 3:
        print("warning: not enough arguments")
        print(
            f"usage: {args[0]} <input_sm_file> <output_mid_file> [additional_output_json_file]"
        )
        print("opening gui mode instead")
        start_gui()

    else:
        input_file = args[1]
        output_file = args[2]
        output_json_file = args[3] if len(args) >= 3 else None
        load_and_convert(input_file, output_file, None, output_json_file)


if __name__ == "__main__":
    main(sys.argv)
