# StepMania to Fortnite Festival Chart Converter

Simple GUI / command-line program for converting StepMania (.sm) files into Fortnite Festival (.mid) files.


## Running the program

You can use the program in one of two ways:
1. Download the `.zip` file from the [Releases](https://gitlab.com/TadeLn/sm2fnf/-/releases) tab, extract it, and run the executable file

or

2. Run the script directly with Python (instructions below)

### Running the script

To run this script, you will need to install MIDIUtil
```
python3 -m pip install MIDIUtil
```

Then you need to execute the `sm2fnf.py` file
```
python3 sm2fnf.py
```


## Usage

### Command-line mode
You can also use the script/executable in a command line. You can provide either 2 or 3 arguments:
```
sm2fnf <input_sm_file> <output_mid_file> [additional_output_json_file]
```

- `input_sm_file` - Path to the `.sm` file that you want to convert
- `output_mid_file` - Path to the resulting `.mid` file
- `additional_output_json_file` (optional) - Path to the resulting `.json` file with the song data read from `.sm`

The first two arguments are necessary. If they are not present, the program launches in GUI mode instead.

**Note:** Currently there is no way to make a custom mapping of charts/difficulties in the command line.
